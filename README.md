# IISNode-notes

First things first ...

## What is Node.js?

Node.js is the JavaScript runtime environment, and includes everything needed to execute a program written in JavaScript.

<img src="https://drive.google.com/uc?export=view&id=1TMIMH-FnUi0oGerGnFK8pOdTUwM3FqBJ" width=60%/><br>

If you know Java, here’s an equivalent relation 👆

## What is IISNode?

To host Node.js applications with the IIS web server, then you'll need to use the "iisnode" IIS module.

Written and developed by Microsoft, under Tomasz Janczuk (https://www.linkedin.com/in/tjanczuk),
iisnode is an open source (https://github.com/Azure/iisnode) wrapper for the JavaScript runtime engine (V8); under the
Apache v2 license (https://en.wikipedia.org/wiki/Apache_License), and is free to use for whatever purpose.
The iisnode module is a proper IIS module, just like ASP.NET and PHP --and is installed as standard for Node.js Apps in Azure #See_the_Note (https://docs.microsoft.com/en-us/azure/app-service/app-service-web-get-started-nodejs#run-the-app-locally)

## Why host Node.JS on Windows and IIS?

Some of the advantages of hosting node.js applications in IIS using the iisnode module as opposed to self-hosting node.exe processes include:

*  **Process management**. The iisnode module takes care of lifetime management of node.exe processes making it simple to improve overall reliability. You don’t have to implement infrastructure to start, stop, and monitor the processes.
*  **Scalability on multi-core servers**. Since node.exe is a single threaded process, it only scales to one CPU core. The iisnode module allows creation of multiple node.exe processes per application and load balances the HTTP traffic between them, therefore enabling full utilization of a server’s CPU capacity without requiring additional infrastructure code from an application developer.
*  **Auto-update**. The iisnode module ensures that whenever the node.js application is updated (i.e. the script file has changed), the node.exe processes are recycled. Ongoing requests are allowed to gracefully finish execution using the old version of the application, while all new requests are dispatched to the new version of the app.
*  **Access to logs over HTTP**. The iisnode module provides access the output of the node.exe process (e.g. generated by console.log calls) via HTTP. This facility is key in helping you debug node.js applications deployed to remote servers.
*  **Side by side with other content types**. The iisnode module integrates with IIS in a way that allows a single web site to contain a variety of content types. For example, a single site can contain a node.js application, static HTML and JavaScript files, PHP applications, and ASP.NET applications. This enables choosing the best tools for the job at hand as well progressive migration of existing applications.
*  **Minimal changes to node.js application code**. The iisnode module enables hosting of existing HTTP node.js applications with very minimal changes. Typically all that is required is to change the listed address of the HTTP server to one provided by the iisnode module via the process.env.PORT environment variable.
*  **Integrated management experience**. The issnode module is fully integrated with IIS configuration system and uses the same tools and mechanism as other IIS components for configuration and maintenance.
In addition to benefits specific to the iisnode module, hosting node.js applications in IIS allows the developer to benefit from a range of IIS features, among them:

    *  port sharing (hosting multiple HTTP applications over port 80)
    *  security (HTTPS, authentication and authorization)
    *  URL rewriting
    *  compression
    *  caching
    *  logging

[*source:* https://tomasz.janczuk.org/2011/08/hosting-nodejs-applications-in-iis-on.html], <br>
[*further detailed:* https://blogs.msdn.microsoft.com/hanuk/2012/05/04/top-benefits-of-running-node-js-on-windows-azure/]

## Installing Node.js on a Windows VPS

### Node.js
Note that if you have Node.js already installed or the executable already on your server, then you can skip this step.

1.  Download and install the latest Node.js installer (MSI) for Windows: https://nodejs.org/en/download/ or a specific version if required https://nodejs.org/dist/
    *  Be sure to install the correct version that matches your server's bitness.
2.  Accept the defaults and finish installing Node.js.

You can confirm that you've successfully installed Node.js by following these steps.

1.  Launch a new command prompt(cmd).
2.  Run 'node --version' and it should print out the Node.js version installed on your server.

### iisnode
If you want to host Node.js applications with IIS, then you'll need to use the "iisnode" IIS module.

1.  Download and install the latest stable iisnode module from: https://github.com/Azure/iisnode/releases
    *  Be sure to install the correct version that matches your server's bitness.
2.  Add "list folder" permissions on c:\home for IIS_IUSRS. You can use the "Advanced" permissions control to apply the permission to "This folder only". Caution: If you're hosting multiple applications for various customers you might need to make further security considerations.
    *  This assumes that you're using the default site path of 'C:\Home\domain.tld' that we configure on Windows VPS servers. If you have a custom setup, then you'll need to be sure that the 'IIS_IUSRS' group has "list folder" permissions for all directories leading up to your site's webroot.

### Configure a site for iisnode
For your Node.js application to work with IIS, you will need to configure the site for use with iisnode. You can accomplish this with some web.config options, or a web.config option as well as a .yml config file. For the sake of simplicity and getting started quickly, we've provided a couple of examples below.

The only changes you'll probably need to make in the example yaml config are the paths to the node.exe executable and the interceptor.

#### web.config:
```xml
<configuration>
<system.webServer>
 
    <handlers>
      <add name="iisnode" path="node_app.js" verb="*" modules="iisnode" />
    </handlers>
 
    <rewrite>
      <rules>
        <rule name="nodejs">
          <match url="(.*)" />
          <conditions>
            <add input="{REQUEST_FILENAME}" matchType="IsFile" negate="true" />
          </conditions>
          <action type="Rewrite" url="/node_app.js" />
        </rule>
      </rules>
    </rewrite>
 
    <security>
      <requestFiltering>
        <hiddenSegments>
          <add segment="node_modules" />
          <add segment="iisnode" />
        </hiddenSegments>
      </requestFiltering>
    </security>
 
  </system.webServer>
</configuration>
```

#### iisnode.yml:
```yml
# The optional iisnode.yml file provides overrides of   
# the iisnode configuration settings specified in web.config.  
  
node_env: production  
nodeProcessCommandLine: "C:\Program Files\nodejs\node.exe"  
interceptor: "C:\Program Files\iisnode\interceptor.js"  
nodeProcessCountPerApplication: 1  
maxConcurrentRequestsPerProcess: 1024  
maxNamedPipeConnectionRetry: 24  
namedPipeConnectionRetryDelay: 250  
maxNamedPipeConnectionPoolSize: 512  
maxNamedPipePooledConnectionAge: 30000  
asyncCompletionThreadCount: 0  
initialRequestBufferSize: 4096  
maxRequestBufferSize: 65536  
watchedFiles: *.js;iisnode.yml  
uncFileChangesPollingInterval: 5000  
gracefulShutdownTimeout: 60000  
loggingEnabled: true  
logDirectory: iisnode  
debuggingEnabled: true  
debuggerPortRange: 5058-6058  
debuggerPathSegment: debug  
maxLogFileSizeInKB: 128  
maxTotalLogFileSizeInKB: 1024  
maxLogFiles: 20  
devErrorsEnabled: false  
flushResponse: false  
enableXFF: false  
promoteServerVars:
```

### Create a simple test application
For the most part, Node.js applications should work as expected. It's important to note that there are some differences. For example, the express .listen() method works differently with iisnode apps, which you can see later in the wiki. If necessary, you can find a handful of example applications in the iisnode github repo: https://github.com/Azure/iisnode/tree/master/src/samples/

### Troubleshooting Your Node.js Application
Logging should be enabled for your application. The standard output and standard error messages should be logged to their own respective log files in the "iisnode" directory.

### Common Errors

#### Error: EPERM: operation not permitted, lstat 'C:\home'

This means that your site's web user doesn't have permission to list the folder contents. Double-check the permissions of your site's web user and make sure that it can list the contents of all directories leading up to your site's web root.

[*source*: https://wiki.hostek.com/Node.js_on_Windows]
